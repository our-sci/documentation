# Our-Sci Documentation

This is the Our-Sci Documentation repository.

Find the documentation over here:
https://gitlab.com/our-sci/documentation/-/jobs/artifacts/master/file/public/index.html?job=pages


# How to edit / deploy

Head over to the wiki page:
https://gitlab.com/our-sci/documentation/wikis/home

There you can create new pages (Home is the main one).

After editing, one more step is necessary to deploy changes.

There is two ways to do this, either just go to this link and hit `Create pipeline`
https://gitlab.com/our-sci/documentation/pipelines/new

Or go to `CI / CD` -> `Pipelines` -> `Run Pipeline` -> `Create pipeline`
Finally go to

https://gitlab.com/our-sci/documentation/-/jobs/artifacts/master/file/public/index.html?job=pages
or even better
https://our-sci.gitlab.io/documentation

after the pipeline ran through (this can take a min or two) to see the updated version.

To change the navigation bar adjust the `sidebar` page in the wiki, it will be used to render the navigation menu.

## Changes
Modify base URL for pages